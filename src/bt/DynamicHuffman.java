
package bt;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class DynamicHuffman {
	
	final int MAX_CHAR = 65536; 
	
	FileInputStream fis;
	BufferedInputStream bis;
	DataInputStream dis;
	
	private FileOutputStream fout;
	private BufferedOutputStream bos;
	private DataOutputStream dos;
	
	String[] huffCode = new String[MAX_CHAR]; // for save the huffman code for each charater
	char[] charDat; // buffer
	
	Node root;
	Node atr;
	Node ptr;
	
	String compData="";
	
	public void initialData() {
		try {
			File fileIn = new File("in.data");
			FileInputStream fis = new FileInputStream(fileIn);
			BufferedInputStream bis = new BufferedInputStream(fis);
			DataInputStream dis = new DataInputStream(bis);
			
			File fileOut = new File("out.data");
			fout = new FileOutputStream(fileOut);
			bos = new BufferedOutputStream(fout);
			dos = new DataOutputStream(bos);
					
			long lenght = dis.available();
			
			charDat = new char[(int) lenght];
			
			root = new Node();
			atr = root;
			ptr = root;
			
			long idx = 0;
			
			
			while (idx < lenght) {
				int kar = dis.read();
				charDat[(int) idx] = (char) kar; //save character to buffer
				
				if (huffCode[kar] == null) {
					addNewNode(kar, atr);
				} else {
					ptr = root;
					updateNode(kar, ptr);
				}
				
				idx++;
			}
                        Node asd = root;
			
			System.out.println("success create tree");
			writeHeader();
			System.out.println("success write file header");
			compress();
			System.out.println("success write compressing");
			
			dos.flush();
		}catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	private void updateNode(int kar, Node ptr2) {
		//System.out.println("update node");
		if (ptr2.leftNode.charNode == kar) {
			ptr2.leftNode.weightNode += 1;
			
			if (ptr2 != root) {
				if (ptr2.leftNode.weightNode > ptr2.parentNode.leftNode.weightNode) {
					swapNode(ptr2);
				}
			}
		} else {
			updateNode(kar, ptr2.rightNode);
		}
	}
	
	private void swapNode(Node ptr2) {
		if (ptr2 != root) {
			if (ptr2.leftNode.weightNode > ptr2.parentNode.leftNode.weightNode) {
				Node temp = ptr2.leftNode;
				
				ptr2.leftNode = ptr2.parentNode.leftNode;
				ptr2.leftNode.parentNode = ptr2;
				ptr2.leftNode.huffCode = ptr2.huffCode + "0";
				huffCode[ptr2.leftNode.charNode] = ptr2.leftNode.huffCode;
				
				ptr2.parentNode.leftNode = temp;
				ptr2.parentNode.leftNode.parentNode = ptr2.parentNode;
				ptr2.parentNode.leftNode.huffCode = ptr2.parentNode.huffCode
						+ "0";
				huffCode[ptr2.parentNode.leftNode.charNode] = ptr2.parentNode.leftNode.huffCode;
				
				swapNode(ptr2.parentNode);
			}
		}
	}
	
	private void addNewNode(int kar, Node atr2) {
		Node newLeaf = new Node(kar, atr2);
		Node newATR = new Node(atr2);
		
		atr2.leftNode = newLeaf;
		atr2.rightNode = newATR;
		
		atr = newATR;
		huffCode[kar] = newLeaf.huffCode;
	}
	
	public void compress() {
		System.out.println("kompresi");
		for(int i = 0; i < charDat.length; i++) {
			compData += huffCode[(int) charDat[i]];
			if(compData.length() > 31) {
				while(compData.length() % 31 != 0) {
					compData = "0" + compData;
				}
				//System.out.println("\nhasil kompresi = " + compData);
				writeCode(compData);
				compData = "";
			}
		}		
		
		System.out.println("berhasil kompresi");
	}
	
	public void writeHeader() {
		try {
			dos.write(charDat.length);
			dos.write(Integer.valueOf("1000000110000001",2)); //this is for delimiter char
			
			for(int i = 0; i < huffCode.length; i++) {
				if(huffCode[i] != null) {
					if(huffCode[i].length() > 31) {
						while(huffCode[i].length() % 31 != 0) {
							huffCode[i] = "0" + huffCode[i];
						}
						String temp = "";
						for(int j = 0; j < huffCode[i].length(); j += 31) {
							int end = j + 31;
							if(end<huffCode[i].length()) {
								temp = huffCode[i].substring(j, end);
								dos.write(Integer.valueOf(temp,2));
							} else {
								temp = huffCode[i].substring(j, huffCode[i].length());
								dos.write(Integer.valueOf(temp,2));
							}
						}
						
						dos.write(Integer.valueOf("1000000110000001",2));
					} else {
						dos.write(Integer.valueOf(huffCode[i],2));
						dos.write(Integer.valueOf("1000000110000001",2));
					}
				}
			}
		   
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		} catch(StringIndexOutOfBoundsException si) {
			si.printStackTrace();
		}
	}
	
	public void writeCode(String compCode) {
		try {
			if(compCode.length() > 31) {
				while(compCode.length() % 31 != 0) {
					compCode = "0" + compCode;
				}
				
				String temp = "";
				for(int i = 0; i< compCode.length(); i += 31) {
					int end = i + 31;
					if(end < compCode.length())
					{
						temp = compCode.substring(i, end);
						dos.write(Integer.valueOf(temp,2));
					}
					else
					{
						temp = compCode.substring(i, compCode.length());
						dos.write(Integer.valueOf(temp,2));
					}
				}
			}else
			{
				dos.write(Integer.valueOf(compCode,2));
			}
			
			//dos.flush();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		} catch(StringIndexOutOfBoundsException si) {
			si.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		DynamicHuffman dh = new DynamicHuffman();
		dh.initialData();
	}
}


